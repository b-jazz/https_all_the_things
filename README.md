HTTPS all the Things
====================

Summary
-------

The main goal of this script it to reduce the time it takes to click on a link in OpenStreetMap. 
The secondary goal is to make https:// more common, recognizable, and hopefully adopted in every 
day behavior. What the script does is takes a small part of the planet
(a [geohash](https://en.wikipedia.org/wiki/Geohash)) and queries [overpass](https://overpass-turbo.eu) 
for some common keys that have http:// links. It will then process those links and see if the website 
owner has published a redirect link so that the user will be sent to the encrypted https version of
their website.

The "common" key(s) at this moment is just "website", but will expand to others like "contact:website" 
and "url" and possibly others. Values that specify neither https nor http will be examined as http to
check for a redirect to https (and updated if found).

Installation
------------

At this point in time, I just run this in a virtual environment using pipenv. If there is ever interest,
I'll try to figure out how to properly package this up and install it so that you can run it without a
venv.

Running
-------

In it's simplest form, you can run it on a very small geohash
```
% mkdir ~/https_all_the_things # first time only to have a place to store working files like the DB
% https_all_the_things --debug --maxedits 5 --workdir ~/https_all_the_things dr5ru3
```

What I do is generate a huge list of geohashes for the contiguous United States, or whatever other area
I'm working on and then loop through those and log them to a file. When the entire batch is done, I'll
remove any geohashes that were completed from the list of geohashes and then I'll run it again. I repeat
this until there are no more geohashes to process. (Yes, I relize this isn't optimal, but it is relatively 
low effort at the moment. I'll automate this all some day. Maybe.)

Here's a quick look at the command that I run:
```
LOGFILE=$(tempfile -d ~/https_all_the_things/logs -p hatt_)
for GEOHASH in $(cat ~/https_all_the_things/unitedstates.geohash); do 
  https_all_the_things --debug --maxedits 10 --wait 10 --workdir ~/https_all_the_things ${GEOHASH} 2>&1 | tee --append ${LOGFILE}
done
```

When that batch finishes, I look for indications of geohashes having been completed. Log entries will
say:
* Found 0 elements
* No http redirects
* Remaining/unexamined objects: 0

I grep for those, pipe them through cut and pull the geohash from the log message. Sort them and then use /usr/bin/comm
against the existing geohash file to remove all completed geohashes. If there are "large" geohashes, ones with many
tags still left to process, I'll remove those top level geohashes and insert the 32 children geohashes into the file and 
run the batch again. Eventually, the list of geohashes will whittle down to nothing.

Tools
-----
A tool I find very handy in the processing of this information is [geoq](https://github.com/worace/geoq). It allows
me to filter geohashes of any degree against a boundary [geojson](https://geojson.io) file. That's how I get the
original list of 4-character geohashes for the contiguous US. And then you can also use it to find out the children
of a given geohash. 

