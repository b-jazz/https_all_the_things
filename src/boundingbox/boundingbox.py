
class BoundingBox:
    """
    Represent a bounding box around a certain area on a map. Provide different representations
    based on the many different standards for how to organize the bounding box parameters.
    """

    def __init__(self, south, west, north, east):
        valid_ranges = [-90.0 <= south <= 90.0,
                        -90.0 <= north <= 90.0,
                        -180.0 <= west <= 180.0,
                        -180.0 <= east <= 180.0,
                        west <= east,
                        south <= north]
        if not all(valid_ranges):
            raise ValueError('Coordinates not within valid ranges and properly ordered.')
        self.south = float(south)
        self.west = float(west)
        self.north = float(north)
        self.east = float(east)

    def __repr__(self):
        return (f'{self.__class__.__name__}(south={self.south}, '
                f'west={self.west}, north={self.north}, east={self.east})')

    def __str__(self):
        """Return a string result in proper order for use with OSM in general."""
        return f'({self.south},{self.west},{self.north},{self.east})'

    def overpass_bbox(self):
        """Return a string result in proper order for use with Overpass QL. User must supply parenthesis if needed."""
        return f'{self.south:.7f},{self.west:.7f},{self.north:.7f},{self.east:.7f}'

    def nominatim_bbox(self):
        """Return a string result in proper order for use with Nominatim. User must supply parenthesis if needed."""
        return f'{self.south},{self.north},{self.west},{self.east}'

    def expand(self, percent="0.1"):
        """
        If you need a slightly larger bounding box for some reason, you can give a percentage amount
        to grow (or shrink if percentage is negative) the bounding box. The new values will be stored.
        The width and height will be expanded by given percentage, not the overall area.
        """
        width_adj = (self.east - self.west) * (percent / 100.0)
        self.east = min(self.east + width_adj, 180.0)
        self.west = max(self.west - width_adj, -180.0)
        height_adj = (self.north - self.south) * (percent / 100.0)
        self.north = min(self.north + height_adj, 90.0)
        self.south = max(self.south - height_adj, -90.0)

    def sql_tuple(self):
        """Useful when passing to a (?,?,?,?) sql query."""
        return (self.south, self.west, self.north, self.east)
