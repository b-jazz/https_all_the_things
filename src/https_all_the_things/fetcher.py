import logging

import requests
import urllib3


# f = Fetcher(http_urls)
# In[0]: f['http://example.com/']
# Out[0]: 'https://example.com/


"""
repeatedly fetch a url until it 200s

f1: http://example.com/ -> 301 https://example.com/
f2: https://example.com/  -> 200
STOP

f1: http://vta.org/ -> 301 http://www.vta.org/
f2: http://www.vta.org/ -> 301 https://www.vta.org/
f3: https://www.vta.org/ -> 200
STOP

f1: http://bogus -> exception
STOP

f1: http://recurse.com -> 301 http://recurse.com/recurse
f2: http://recurse.com/recurse -> 301 http://recurse.com/recurse/recurse
f3: http://recurse.com/recurse/recurse -> 301 http://recurse.com/recurse/recurse/recurse/recurse
f4: ABORT
"""


class Response(object):
    def __init__(self, response=None, status=0, exception=None):
        if response:
            self.response = response
            self.final_url = response.request.url
            self.status_code = response.status_code
        else:
            self.status_code = status
            self.exception = exception

    def location(self):
        if self.status_code in (requests.codes.moved_permanently, requests.codes.found):
            return self.response.headers.get("Location", None)
        else:
            raise KeyError("Object not a redirect response.")

    def status(self):
        return self.status_code


class Fetcher(object):
    def __init__(self, urls, geohash, stats, timeout, headers):
        self.log = logging.getLogger(__name__)
        self.urls = urls
        self.geohash = geohash
        self.stats = stats
        self.timeout = timeout
        self.headers = headers
        self.fetches = {}
        self.perform_fetches()
        self.okays = {
            url: self.fetches[url]
            for url in self.fetches
            if self.fetches[url].status_code == requests.codes.okay
        }
        self.log.debug(f"LENGTH OF OKAYS: {len(self.okays)}")
        self.log.debug(f"LENGTH OF FETCHESDB: {len(self.fetches)}")

        # the following shouldn't happen now that we're turning on redirects and looking for the final redirect in
        # response.request.url instead of response.headers['Location']
        #
        # self.redirects = {url: self.fetches[url] for url in self.fetches
        #                   if self.fetches[url].status in (requests.codes.moved_permanently, requests.codes.found)}

    def perform_fetches(self):
        for url in list(set(self.urls)):
            try:
                self.log.debug(f"{self.geohash}:::{url}::Fetching HEADers")
                self.stats.increment("HEADs")
                response = requests.head(
                    url,
                    timeout=self.timeout,
                    headers=self.headers,
                    allow_redirects=True,
                )
            except (
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
                UnicodeError,
            ) as ex:
                self.log.error(
                    (
                        f"{self.geohash}:::{url}::"
                        f"exception while fetching: {type(ex)}: {ex}"
                    )
                )
                self.fetches[url] = Response(status=0, exception=ex)
                continue
            except (
                requests.exceptions.InvalidURL,
                requests.exceptions.InvalidSchema,
            ) as ex:
                self.log.error(
                    f"{self.geohash}:::{url}::Broken URL (InvalidSchema or InvalidURL)"
                )
                self.stats.increment("errors")
                self.fetches[url] = Response(status=0, exception=ex)
                continue
            except (
                urllib3.exceptions.InvalidHeader,
                urllib3.exceptions.LocationParseError,
            ) as ex:
                self.log.error(
                    f"{self.geohash}:::{url}::urllib3 exception. {type(ex)} - {ex}"
                )
                self.fetches[url] = Response(status=0, exception=ex)
                continue
            else:
                # self.log.debug('NO HEAD EXCEPTIONS')
                self.fetches[url] = Response(response=response)


## TODO: refactor and name the dictionary "responses", not "fetches".
