# TODO:
#
# * handle multiple values in website tag, or at the very least, just punt on them
# * expand search to include schema-less urls (more appropriate for KeepRight?)
# * use grequests to fetch headers asyncronously
# * put the known empty geohashes in a bloom filter instead of json file
#

from collections import Counter
from datetime import datetime, timedelta
import json
import logging
import os.path
import random
import re
import sys
import time
import urllib3

from bloom_filter import BloomFilter
import click
import geohash2
import osmapi
import overpass
import requests
# from requests_oauth2client import OAuth2Client, OAuth2AuthorizationCodeAuth
from requests_oauthlib import OAuth2Session

from boundingbox.boundingbox import BoundingBox


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i+n]


class HTTPSOSM(object):
    def __init__(
        self, debug, geohash, chunksize, maxchangesets, timeout, wait, workdir
    ):
        """App Object. This controls everything."""
        self.setup_logging(debug)
        self.user_agent = "https_all_the_things/0.2.0"
        self.chunksize = chunksize
        self.maxchangesets = maxchangesets
        self.maxedits = chunksize * maxchangesets
        self.timeout = timeout
        self.wait = wait
        self.workdir = os.path.expandvars(os.path.expanduser(workdir))

        client_id_key = "HATT_OAUTH2_CLIENT_ID"
        token_str_key = "HATT_OAUTH2_TOKEN_STR"
        if os.environ.get(client_id_key) and os.environ.get(token_str_key):
            self.token_str = os.environ[token_str_key]
            self.client_id = os.environ[client_id_key]
        else:
            raise ValueError(f"{client_id_key} and/or {token_str_key} environment variables not set.")

        if re.match("^[0-9b-hjkmnp-z]{2,20}$", geohash):
            if len(geohash) == 2 and self.chunksize != 1:
                raise ValueError(
                    f"You are only allowed to use 2 digit geohashes when chunksize is set to 1"
                )
            else:
                self.geohash = geohash
        else:
            raise ValueError(
                f"Geohash {geohash} is too long, too short, or contains invalid characters."
            )
        self.longterm_bloom = BloomFilter(
            max_elements=1000000,
            error_rate=0.1,
            filename=f"{self.workdir}/longterm.bloom",
        )
        self.shortterm_bloom = BloomFilter(
            max_elements=1000000,
            error_rate=0.1,
            filename=f"{self.workdir}/shortterm.bloom",
        )
        try:
            self.empties = json.load(open(f"{self.workdir}/empties.geohash", "r"))
        except Exception:
            self.empties = []

    def setup_logging(self, debug):
        log_format = "%(asctime)s {0}".format(logging.BASIC_FORMAT)
        if debug:
            # we want to turn on debug level logging. also, leave the requests logger at debug as well.
            logging.basicConfig(format=log_format, level=logging.DEBUG)
        else:
            logging.basicConfig(format=log_format)
            # if we aren't debugging, quiet down the requests logger
            logging.getLogger("requests").setLevel(logging.ERROR)
            logging.getLogger("osmapi.dom").setLevel(logging.ERROR)

        logging.getLogger("urllib3").setLevel(logging.WARNING)
        self.log = logging.getLogger(__name__)

    def compute_bbox(self):
        (lat, lon, deltalat, deltalon) = geohash2.decode_exactly(self.geohash)
        self.bbox = BoundingBox(
            north=lat+deltalat,
            south=lat-deltalat,
            west=lon-deltalon,
            east=lon+deltalon,
        )
        self.log.debug(f"{self.geohash}:::::Computed bounding box of: {self.bbox}")

    def find_websites(self):
        # Returns:
        # [{'type': 'node', 'id': 1597046045,
        #   'tags': {'name': 'Cerro Azul', 'tourism': 'hotel',
        #            'website': 'http://www.hostalisbelagalapagos.com/'}}, ...]
        opquery = f'[out:json][maxsize:1073741824];(nwr["website"]({self.bbox.overpass_bbox()}); -nwr["website"~"https://"]({self.bbox.overpass_bbox()}););out tags;'
        self.log.debug(f"{self.geohash}:::::Overpass query: {opquery}")
        opapi = overpass.API(timeout=120)
        try:
            opresults = opapi.get(opquery, build=False)
        except (
            overpass.MultipleRequestsError,
            overpass.ServerLoadError,
            overpass.errors.TimeoutError,
            requests.exceptions.ConnectionError,
        ) as ex:
            self.log.error(
                f"{self.geohash}:::::Overpass is having problems. Backing off for a while. {ex}"
            )
            time.sleep(300.0)
            sys.exit(1)
        self.log.debug(
            f'{self.geohash}:::::Found {len(opresults["elements"])} elements from Overpass query'
        )
        if len(opresults["elements"]) == 0:
            # add to empties list and write json back out to file
            self.empties.append(self.geohash)
            json.dump(self.empties, open(f"{self.workdir}/empties.geohash", "w"))
            sys.exit(0)
        return opresults["elements"]

    def find_https_redirects(self, elements):
        """Filter out websites that are in the short/long term bloom filters. Clean up
        the URLs so that they are normalized with http:// at the start. Start fetching
        websites and collect redirects, stats, and add to bloom filters as necessary."""

        # zeroing out the entries will help set an order to the dictionary (apparently)
        stats = {
            "fetched": len(elements),
            "examined": 0,
            "longhits": 0,
            "shorthits": 0,
            "fetches": 0,
            "bigdiff": 0,
            "errors": 0,
            "multikeys": 0,
            "unexamined": 0,
            "update": 0,
        }
        https_elements = []

        # randomize the elements in case overpass isn't doing it for us. no need to query the same old
        # non-redirecting websites over and over in a large sample.
        random.shuffle(elements)

        filtered_elements = []
        for element in elements:
            website = element["tags"]["website"]
            if "http://" not in website:
                # we have expanded to looking for "example.com" along with "http://example.com"
                # and we should prefix the website so later comparisons with https://* works later
                self.log.debug(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"Converting protocol-less URL to force http"
                    )
                )
                website = "http://" + website
            if website in self.longterm_bloom:
                self.log.debug(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"Long term bloom filter hit. Did not redirect previously."
                    )
                )
                stats["examined"] += 1
                stats["longhits"] += 1
                continue
            if website in self.shortterm_bloom:
                self.log.debug(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"Short term bloom filter hit Skipping."
                    )
                )
                stats["examined"] += 1
                stats["shorthits"] += 1
                continue
            if Counter(element["tags"].values())[website] > 1:
                # Check that not more than 1 *value* are the same. For instance, if "website"
                # and "url" both point to "http://example.com", we don't want to change just the
                # "website" tag to be "https://example.com".
                self.log.error(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"Multiple keys have identical values of website. Skip to remain consistent."
                    )
                )
                stats["examined"] += 1
                stats["multikeys"] += 1
                continue
            self.log.debug(
                (
                    f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                    f"Website not pre-filtered. Adding to filtered list."
                )
            )
            filtered_elements.append(element)

        for element in filtered_elements:
            stats["examined"] += 1
            try:
                website = element["tags"]["website"]
                if "http://" not in website:
                    # we have expanded to looking for "example.com" along with "http://example.com"
                    # and we should prefix the website so later comparisons with https://* works later
                    self.log.debug(
                        (
                            f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                            f"Converting protocol-less URL to force http"
                        )
                    )
                    website = "http://" + website
                self.log.debug(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"Fetching HEADers"
                    )
                )
                stats["fetches"] += 1
                head = requests.head(
                    website,
                    timeout=self.timeout,
                    headers={"User-Agent": self.user_agent},
                )
            except (
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
                UnicodeError,
            ) as ex:
                self.log.error(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"exception while fetching: {type(ex)}: {ex}"
                    )
                )
                self.log.debug(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"Adding to short term bloom filter"
                    )
                )
                stats["shorthits"] += 1
                self.shortterm_bloom.add(website)
                continue
            except (
                requests.exceptions.InvalidURL,
                urllib3.exceptions.LocationParseError,
                requests.exceptions.InvalidSchema,
            ):
                self.log.error(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f'FIXME http://level0.osmz.ru/?url={element["type"][0]}{element["id"]}'
                    )
                )
                stats["errors"] += 1
                continue
            except urllib3.exceptions.InvalidHeader:
                self.log.error(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"urllib3 exception."
                    )
                )
                stats["errors"] += 1
                continue

            if head.status_code == requests.codes.ok:
                self.log.debug(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"(200) Adding to long term bloom filter"
                    )
                )
                stats["longhits"] += 1
                self.longterm_bloom.add(website)
            elif head.status_code in (
                requests.codes.moved_permanently,
                requests.codes.found,
            ):
                try:
                    new_location = head.headers["Location"]
                except KeyError:
                    stats["errors"] += 1
                    self.log.debug(
                        (
                            f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                            f"Missing Location: header line even though it is a redirect status code"
                        )
                    )
                    continue
                if re.match("^https://", new_location):
                    self.log.debug(
                        (
                            f'{self.geohash}:{element["type"]}:{element["id"]}:{website}:{new_location}:'
                            f"HTTPS redirect ({head.status_code}) found"
                        )
                    )
                    if any(
                        (
                            website.replace("http://", "https://", 1) == new_location,
                            website.replace("http://", "https://", 1) + "/"
                            == new_location,
                            website.replace("http://", "https://www.", 1)
                            == new_location,
                            website.replace("http://", "https://www.", 1) + "/"
                            == new_location,
                            website.replace("http://www.", "https://", 1)
                            == new_location,
                            website.replace("http://www.", "https://", 1) + "/"
                            == new_location,
                        )
                    ):
                        element["tags"]["website"] = new_location
                    else:
                        self.log.debug(
                            (
                                f'{self.geohash}:{element["type"]}:{element["id"]}:{website}:{new_location}:'
                                f"Skipping. Redirect too different from original"
                            )
                        )
                        stats["bigdiff"] += 1
                        self.log.debug(
                            (
                                f'{self.geohash}:{element["type"]}:{element["id"]}:{website}:{new_location}:'
                                f"Adding to short term bloom filter"
                            )
                        )
                        self.shortterm_bloom.add(website)
                        stats["shorthits"] += 1
                        continue
                    https_elements.append(element)
                    self.maxedits -= 1
                    if self.maxedits <= 0:
                        self.log.fatal(
                            (
                                f"{self.geohash}:::::"
                                f"Hit max number of edits to make ({len(https_elements)}). Exiting without writing. Split this geohash."
                            )
                        )
                        sys.exit(-1)
                else:
                    self.log.error(
                        (
                            f'{self.geohash}:{element["type"]}:{element["id"]}:{website}:{new_location}:'
                            f"Redirect not to a secure protocol"
                        )
                    )
                    self.log.debug(
                        (
                            f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                            f"Adding to short term bloom filter"
                        )
                    )
                    stats["shorthits"] += 1
                    self.shortterm_bloom.add(website)
            else:
                self.log.error(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"Received unexpected result code ({head.status_code}) from {website}"
                    )
                )
                self.log.debug(
                    (
                        f'{self.geohash}:{element["type"]}:{element["id"]}:{website}::'
                        f"Adding to short term bloom filter"
                    )
                )
                stats["shorthits"] += 1
                self.shortterm_bloom.add(website)

        stats["update"] = len(https_elements)
        stats["unexamined"] = len(elements) - stats["examined"]
        self.log.debug((f"{self.geohash}:::::" f"Stats: {stats}"))
        return https_elements

    def fresh(self, osm_obj, hours):
        if datetime.now() - osm_obj["timestamp"] < timedelta(hours=hours):
            return True
        return False

    def update_objects(self, candidates):
        # connect to apis
        api_url = "https://api.openstreetmap.org"
        oauth_session = OAuth2Session(self.client_id, token={"access_token": self.token_str})
        mapapi = osmapi.OsmApi(
            api=api_url,
            session=oauth_session,
        )
        self.log.debug(f"{self.geohash}:::::Created OSM API to {api_url}")

        # set up mapping of object types (node/way/relation) and get/update functions
        obj_get = {
            "node": mapapi.NodeGet,
            "way": mapapi.WayGet,
            "relation": mapapi.RelationGet,
        }
        obj_update = {
            "node": mapapi.NodeUpdate,
            "way": mapapi.WayUpdate,
            "relation": mapapi.RelationUpdate,
        }

        # open changeset
        changeset_tags = {
            "created_by": self.user_agent,
            "comment": "Find all the http:// website tags in OSM that redirect to https:// and update them.",
            "bot": "yes",
            "description": "https://wiki.openstreetmap.org/wiki/Automated_Edits/b-jazz",
        }
        mapapi.ChangesetCreate(changeset_tags)
        changes_applied = False

        for candidate in candidates:
            obj_type = candidate["type"]
            obj_id = candidate["id"]

            osm_obj = obj_get[obj_type](obj_id)

            if self.fresh(osm_obj, 72):
                self.log.error(
                    (
                        f"{self.geohash}:{obj_type}:{obj_id}:"
                        f'{osm_obj["tag"]["website"]}:{candidate["tags"]["website"]}:'
                        f"Skipping due to object being too new and possibly actively being modified"
                    )
                )
                continue
            if "https://" in osm_obj["tag"]["website"]:
                self.log.fatal(
                    (
                        "FOUND HTTPS ALREADY EXISTS. We must have recently updated the object "
                        "but it spans multiple geohashes and overpass has not had a chance to sync up. "
                        "Exit and try again on another pass."
                    )
                )
                sys.exit(1)

            if len(candidate["tags"]["website"]) <= 255:
                osm_obj["tag"]["website"] = candidate["tags"]["website"]
                self.log.debug(
                    (
                        f"{self.geohash}:{obj_type}:{obj_id}:"
                        f'{osm_obj["tag"]["website"]}:{candidate["tags"]["website"]}:'
                        f"Updating object via osmapi: {osm_obj}"
                    )
                )
                obj_update[obj_type](osm_obj)
                changes_applied = True
            else:
                self.log.error(
                    (
                        f"{self.geohash}:{obj_type}:{obj_id}:"
                        f'{osm_obj["tag"]["website"]}:{candidate["tags"]["website"]}:'
                        f"Website value too large. Ignoring."
                    )
                )

        if changes_applied:
            changesetid = mapapi.ChangesetClose()
            self.log.debug(
                f"{self.geohash}:::::Changeset closed. https://osm.org/changeset/{changesetid}"
            )
        self.log.debug(f"{self.geohash}:::::Sleeping {self.wait} seconds...")
        time.sleep(self.wait)

    def run(self):
        self.log.debug(f"{self.geohash}:::::Processing geohash")
        if self.geohash in self.empties:
            self.log.debug(f"{self.geohash}:::::Known empty hash")
            sys.exit(0)
        self.compute_bbox()
        website_elements = self.find_websites()
        https_candidates = self.find_https_redirects(website_elements)
        if https_candidates:
            for chunk in chunks(https_candidates, self.chunksize):
                self.update_objects(chunk)
        else:
            self.log.debug(f"{self.geohash}:::::No https redirects found")


@click.command(context_settings=dict(help_option_names=["-h", "--help"]))
@click.option(
    "--debug",
    default=False,
    is_flag=True,
    help="Outputs additional debugging information",
)
@click.option(
    "--chunksize", default=10, help="How many n/w/r edits should be done per changeset"
)
@click.option(
    "--maxchangesets",
    default=10,
    help="Quit (without writing anything) if there are more than chunksize * maxchangesets of edits to do.",
)
@click.option(
    "--timeout", default=15.0, help="Connection and Read timeout value for requests"
)
@click.option(
    "--wait", default=5.0, help="How long to wait after closing the changeset"
)
@click.option(
    "--workdir",
    default="/tmp",
    help="Where to look for and store files like the bloom filter db",
)
@click.argument("geohash", type=str)
def main(debug, geohash, chunksize, maxchangesets, timeout, wait, workdir):
    """Find all of the website tags with http in the value and convert to https if they 301 to an https url"""
    app = HTTPSOSM(debug, geohash, chunksize, maxchangesets, timeout, wait, workdir)
    app.run()


if __name__ == "__main__":
    main()
