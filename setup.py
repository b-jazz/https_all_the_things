#!/usr/bin/env python
import setuptools

setuptools.setup(
    name='https_all_the_things',
    version='0.2.0',
    description='Find http redirecting to https and update OSM',
    url='https://github.com/b-jazz',
    license='MIT',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python :: 3',
    ],
    package_dir={'':'src'},
    packages=setuptools.find_packages('src'),
    install_requires=[
        'bloom_filter',
        'click',
        'geohash2',
        'osmapi',
        'overpass',
        'requests',
        'requests_oauth2client',
        'requests_oauthlib',
    ],
    entry_points = {
        'console_scripts': [
            'https_all_the_things=https_all_the_things.httpsosm:main',
        ],
    },
)
