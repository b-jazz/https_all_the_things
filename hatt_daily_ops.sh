#!/usr/bin/env bash

#TEMPFILE=$(tempfile -d ~/osm/hatt/logs -p hatt_)
#for GH in $(cat $(/bin/ls ~/osm/hatt/non-us.geohash.* | sort | tail -1)); do
#  https_all_the_things --debug --maxedits 10 --wait 5 --workdir ~/osm/hatt ${GH} 2>&1 | tee --append ${TEMPFILE}
#done

TEMPFILE=$(mktemp /home/jasmerb/osm/hatt/logs/hatt_XXX)
rm -f /home/jasmerb/osm/hatt/logs/latest
ln -s $TEMPFILE /home/jasmerb/osm/hatt/logs/latest

OVERRIDESLEEP=/home/jasmerb/osm/hatt/OVERRIDESLEEP.txt

GHFILE=$(/bin/ls /home/jasmerb/osm/hatt/planet.2digit.geohash.v* | sort | tail -1)
rm -f /home/jasmerb/osm/hatt/latest.geohash
ln -s $GHFILE /home/jasmerb/osm/hatt/latest.geohash

mapfile -t GHS < <(shuf "$GHFILE")

for GH in "${GHS[@]}"; do
    STARTSEC=$(date +%s)
    SLEEPSEC=$((60*60*24*7 / ${#GHS[@]}))
    if [ -f $OVERRIDESLEEP ]; then
	NEWSLEEPSEC=$(head -1 $OVERRIDESLEEP)
	if (( $NEWSLEEPSEC > 0 )); then
	    SLEEPSEC=$NEWSLEEPSEC
	fi
    fi
    SLEEPUNTIL=$((STARTSEC + SLEEPSEC))
    /home/jasmerb/.local/bin/https_all_the_things --debug --chunksize 50 --maxchangesets 500 --wait 1 --workdir /home/jasmerb/osm/hatt $GH >> "$TEMPFILE" 2>&1
    ENDSEC=$(date +%s)

    echo "sleepsec: $SLEEPSEC, start: $STARTSEC, sleepuntil: $SLEEPUNTIL, endsec: $ENDSEC" >> "$TEMPFILE"
    if [[ $SLEEPUNTIL > $ENDSEC ]]; then
        NEWSLEEP=$((SLEEPUNTIL - ENDSEC))
        echo "sleeping $NEWSLEEP seconds..." >> "$TEMPFILE"
        sleep $NEWSLEEP
    else
        echo "Processing took long enough. Not sleeping!" >> "$TEMPFILE"
    fi
done

# useful:
#ack "\x80\x8E"  ~/osm/hatt/logs/hatt_* | cut -d: -f8-11 | awk -F: '{printf "http://level0.osmz.ru/?url=%s%s\t%s:%s\n", $1, $2, $3, $4}' | sed -e 's/way/w/' -e 's/node/n/' -e 's/relation/r/' | sort | uniq | grep .http | less
